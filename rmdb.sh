#!/bin/bash
# Re-make database. Useful if you are a dummy and keep changing things.

sudo -u postgres psql -c "drop database karl"||:
sudo -u postgres psql -c "drop database test_karl" ||:
rm -rf ./media/*
rm -rf ./morkitemine/mines/migrations/*
touch ./morkitemine/mines/migrations/__init__.py

sudo -u root $(dirname $0)/install.sh
