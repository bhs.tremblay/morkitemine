======================================
The MorkiteMine Programming Challenge
======================================

This is the main repository for the MorkiteMine programming challenge,
which is inspired by Firebase's GoldMine programming challenge as described
by Andrew Lee in `this blog post`_.

Powered by

- `Django`_
- `NetworkX`_

and with special thanks to `Nick`_ for solving this problem several times over
throughout its development.

You can read about the development of the project on my site `here`_.

.. tip:
    If you are looking to actually *do* the challenge,
    see the README in the `challenge` directory.

    This document covers using the MorkiteMine Django app for administrating the
    challenge.


The MorkiteMine Orchestrator (or: Developer Documentation Nobody Will Ever Read)
================================================================================

The ``morkitemine`` Django project has only one app, ``mines``,
which has just 4 models and 3 core management commands.
They correspond to the following components of administrating the challenge:

- **Creating Morkite mines:** the ``Mine`` and ``Quarry`` models, and the ``excavate`` command.
- **Registering solution programs:** the ``Solution`` model, and the ``register`` command.
- **Computing and reviewing results:** the ``Result`` model, and the ``results`` command.

Mines
------

*Models:* ``Mine``, ``Quarry``

*Management commands:* ``excavate``

Morkite mines are simple, connected, undirected, geometric graphs
with vertex values and edge costs.

To generate a mine, the coordinates for 249 vertices are chosen uniformly at
random in the 2-dimensional unit cube, and the root vertex is placed at the
origin.
Then, the radius is grown incrementally until a configured minimum graph density
is realized, at which point any connected component not containing the root
is thrown out.
Finally, vertex values and edge costs are chosen independently at random
from the integers 1 to 100.
Vertex values are chosen with frequency inverse to their value,
i.e. the higher the value the lower the probability it will be picked.
Edge costs are chosen uniformly at random.
The ``generate_morkite_mine`` method is a good place to start looking for
implementation details.

So, each mine is defined by four parameters:

#. The random seed for generating graph coordinates.
#. The random seed for generating vertex values.
#. The random seed for generating edge costs.
#. The minimum density.

.. warning::

    Changing the radius increment value may change the mine defined by these
    parameters, making future results incomparable to older results, and
    damaging data integrity in general.

The ``Mine`` model requires all four parameters and enforces uniqueness together.

The ``Quarry`` model requires a list of each of these four parameters,
and defines a group of mines from all valid combinations.
Mines can belong to multiple quarries.

The ``excavate`` command takes a set of each of these four parameters and
defines a quarry.

Examples
^^^^^^^^^

Consider the following command::

    ./manage.py excavate --graph-seed 0 --graph-seed 1 --value-seed 0 --value-seed 1 --cost-seed 0 --density 0.03

This defines a quarry having the configuration

- Graph seeds: 0, 1
- Value seeds: 0, 1
- Cost seeds: 0
- Densities: 0.03

and hence four mines, i.e.

- Mine 1:
    - Graph seed: 0
    - Value seed: 0
    - Cost seed: 0
    - Density: 0.03

- Mine 2:
    - Graph seed: 1
    - Value seed: 0
    - Cost seed: 0
    - Density: 0.03

- Mine 3:
    - Graph seed: 0
    - Value seed: 1
    - Cost seed: 0
    - Density: 0.03

- Mine 4:
    - Graph seed: 1
    - Value seed: 1
    - Cost seed: 0
    - Density: 0.03

If the following command is run next::

    ./manage.py excavate --graph-seed 0 --graph-seed 1 --value-seed 0 --cost-seed 0 --density 0.03

then it will define a second quarry having only the first two mines.

.. tip::

    At least one of each seed is required, but a default single density
    (configurable in settings) will be used if none are provided.


Solutions
----------

*Models:* ``Solution``

*Management commands:* ``register``

Solutions, or *submissions*, are programs.
They must be made so that the MorkiteMine orchestrator can see them and
has permission to execute them.

The ``Solution`` model has two required arguments, and a few others for
categorizing the solution:

- name (a name to refer to the solution by, required)
- path (the absolute path to the executable on the system, required)
- division (the competitive class of the solution)
- author (the candidate to credit the solution to)

The division can be null for now, but in the future it should be required.
Solutions can realistically only be compared to other solutions in the same
competitive division. At the time of writing, divisions are 'compiled'
(i.e. C, C++, C#) and 'interpreted' (i.e. Python).

Examples
^^^^^^^^^

The ``register`` is quite self-explanatory::

    ./manage.py register greedywalk /home/bentremblay/miners/greedy.py --author ben --division i

This creates a solution with ``greedywalk`` as the name and
``/home/bentremblay/miners/greedy.py`` as the absolute path to the executable
the solution represents. The author and division fields have also been specified.

If a solution with this name already exists, or if it is determined that the
given path does not specify an existing file executable by the orchestrator,
the command will fail with a warning.


Results
--------

*Models:* ``Result``

*Management commands:* ``results``

As you would expect, a result is the outcome of solving a MorkiteMine problem
instance with a particular solution.

A MorkiteMine problem instance is not defined by a mine alone,
but the combination of a mine and an *edge cost allowance*
(henceforth refered to as an *ECA* for short).

Furthermore, a MorkiteMine solution does not (necessarily) return on its own,
but rather may run indefinitely, continuining to print improved output
until we stop it.

As such, a result is defined by the combination of four parameters:

- mine
- EHP (defines an ECA)
- solution
- time (seconds of program run time)

The *EHP* is the *expected Hamiltonian cycle cost percentage*
(so the full acronym is *EHCCP* but who has time for that).
As the name implies, it defines an ECA for the given mine equal to a percentage
of the mine's expected Hamiltonian cycle cost.
For example, if a mine has an expected Hamiltonian cycle cost of 1000 and the
EHP is 20, then an ECA of 200 is used.

The time is the number of seconds to run the solution program on the problem
instance for.

Given these parameters, there are three stored result fields:

- output
- score
- error

The ``output`` is the last (complete) output read from the program before
``time`` seconds. The score and errors are computed from the output and stored
for convenience.

To receive a score, the output is first validated (correct format, legal moves,
etc.).
If a validation assertion fails, the error is saved and a score of zero
is assigned.
If validation passes, no error is saved (i.e. the field will be null) and
a score equal to the value of the output divided by the total value of the mine
is assigned.

Examples
^^^^^^^^^

The ``results`` management command takes multiple mine and/or quarry ids,
solution names, EHPs, and times. Any missing results are computed, and
two forms of result visualizations are returned:

- for each mine/time pair, a table comparing solution scores per-eca is printed
- a plot comparing solution scores per-eca (summed over each mine/time pair) is saved

For example, consider the following ``results`` call::

    ./manage.py results --quarry 4 --mine 7 --ehp 5 --ehp 10 --solution greedywalk --solution generouswalk --time 30 --time 60

This will run the *greedywalk* and *generouswalk* solutions on the problem
instances obtained by combining all mines specified
(i.e. all mines in quarry 4 and mine 7)
with all EHPs specified (i.e. 5 and 10),
and saving outputs at 30 and 60 seconds of program run time.


Everything Else
-----------------

Those are the models and all the commands for using them.

There is also a ``draw`` command to visualize a mine. It draws edges on a
green-yellow-red gradient, with green being lower cost and red being higher
cost, and vertices on a blue-gradient, with darker vertices having a higher
value.

There is another command, ``autoregister``, which automatically registers
all solutions in a configurable directory. Useful if you are deploying a new
MorkiteMine orchestrator and already have a bank of solutions to register.

Finally, to *actually give the challenge to someone*,
just send them a tarball of the challenge directory.
I don't have instructions for getting them to actually do it, though. Beg?

.. _this blog post: https://startupandrew.com/posts/how-firebase-interviewed-software-engineers/
.. _Django: https://www.djangoproject.com/
.. _Networkx: https://networkx.github.io/
.. _Nick: https://nickhuber.ca/
.. _here: https://tremblay.dev/tags/morkitemine/

.. _SilverMine: https://gitlab.com/bhs.tremblay/silvermine
.. _geometric graph: https://networkx.github.io/documentation/networkx-2.4/reference/generated/networkx.generators.geometric.random_geometric_graph.html
.. _random.choices: https://docs.python.org/3/library/random.html#random.choices
