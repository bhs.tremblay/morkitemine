import os
import math
import numpy
import random
import subprocess
import time
import threading
import networkx as nx
from django.db import models
from django.conf import settings
from django.contrib.postgres.fields import ArrayField, JSONField
from django.core.exceptions import ObjectDoesNotExist
from mines.io import write_mine, draw_mine, read_walk_string
from tempfile import NamedTemporaryFile
import queue

# CHANGING THESE INVALIDATES MINES AND RESULTS!
MAX_ORDER = 250
MAX_SIZE = 31125
RADIUS_INCREMENT = 0.01
DIMENSION = 2
VALUE_POPULATION = list(range(1, 101))
VALUE_WEIGHTS = list(reversed(VALUE_POPULATION))
COST_POPULATION = list(range(1, 101))


class Mine(models.Model):
    # Configuration
    graph_seed = models.PositiveIntegerField()
    value_seed = models.PositiveIntegerField()
    cost_seed = models.PositiveIntegerField()
    density = models.DecimalField(max_digits=4, decimal_places=3) #  TODO: validate this is in [0, 1]
    # Cache
    graph = JSONField(null=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=[
                    'graph_seed',
                    'value_seed',
                    'cost_seed',
                    'density',
                ],
                name='unique-mine',
            ),
        ]

    def clear_graph(self):
        self.graph = None
        self.save()

    def set_graph(self, graph):
        self.graph = nx.node_link_data(graph)
        self.save()

    def is_generated(self):
        return self.graph is not None

    def get_graph(self):
        if not self.is_generated():
            graph = generate_morkite_mine(
                graph_seed=self.graph_seed,
                value_seed=self.value_seed,
                cost_seed=self.cost_seed,
                density=self.density,
            )
            self.set_graph(graph)
        return nx.node_link_graph(self.graph)

    # TODO: these two methods shouldn't force you into the default media
    # and mines directories; those should be overwritable defaults.
    def write(self):
        filepath = settings.MINES_DIR + f'/{self.id}.mine'
        return write_mine(filepath, self.get_graph())

    def draw(self):
        filepath = settings.MEDIA_DIR + f'/{self.id}.png'
        return draw_mine(filepath, self.get_graph())

    def get_ehcc(self):
        graph = self.get_graph()
        expected_edge_cost = graph.size(weight='cost') / graph.size()
        expect_hamiltonian_cycle_cost = graph.order() * expected_edge_cost
        return expect_hamiltonian_cycle_cost

    def get_value(self):
        graph = self.get_graph()
        graph_value = sum(nx.get_node_attributes(graph, 'value').values())
        return graph_value


class Quarry(models.Model):
    """
    A set of seeds (and possibly densities) that can be combined to define many mines.
    """
    # Configuration
    graph_seeds = ArrayField(models.PositiveIntegerField(), size=10)
    value_seeds = ArrayField(models.PositiveIntegerField(), size=10)
    cost_seeds = ArrayField(models.PositiveIntegerField(), size=10)
    densities = ArrayField(models.DecimalField(max_digits=4, decimal_places=3), size=10)
    # Cache
    mines = models.ManyToManyField(Mine)

    # TODO: enforce uniqueness on configuration fields

    def excavate(self):
        mines = []
        for density in self.densities:
            for graph_seed in self.graph_seeds:
                for value_seed in self.value_seeds:
                    for cost_seed in self.cost_seeds:
                        m, _ = Mine.objects.get_or_create(
                            graph_seed=graph_seed,
                            value_seed=value_seed,
                            cost_seed=cost_seed,
                            density=density,
                        )
                        self.mines.add(m)
                        mines.append(m)
        return mines


class Solution(models.Model):
    name = models.CharField(max_length=200, unique=True)
    # TODO: forcing solutions to be in a particular directory is an unnecessary and awkward restriction...
    path = models.FilePathField(path=settings.SOLUTIONS_DIR, unique=True)
    author = models.CharField(max_length=200, null=True)
    division = models.CharField(max_length=200, null=True)

    def __str__(self):
        return f'{self.name}'

    def is_present(self):
        return os.path.isfile(self.path)

    def is_executable(self):
        return os.access(self.path, os.X_OK)

    def is_valid(self):
        return self.is_present() and self.is_executable()


class Result(models.Model):
    mine = models.ForeignKey(Mine, on_delete=models.CASCADE)
    ehp = models.PositiveIntegerField()
    solution = models.ForeignKey(Solution, on_delete=models.CASCADE)
    time = models.PositiveIntegerField()
    output = models.TextField(null=True)
    score = models.DecimalField(max_digits=5, decimal_places=4)
    error = models.CharField(max_length=200, null=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=[
                    'mine',
                    'solution',
                    'ehp',
                    'time',
                ],
                name="unique-result",
            ),
        ]


def get_eca(mine, ehp):
    """
    Given a mine's EHPC and a percentage (positive integer),
    calculate and return that percentage of the mine's EHPC
    """
    return int(mine.get_ehcc() * (ehp / 1E2))


def set_seed(seed):
    random.seed(seed)
    numpy.random.seed(seed)


def generate_morkite_mine(density, graph_seed, value_seed, cost_seed):
    graph = generate_graph(density, graph_seed)
    graph = add_values(graph, value_seed)
    graph = add_costs(graph, cost_seed)
    return graph


def generate_graph(density_minimum, graph_seed):
    set_seed(graph_seed)
    pos = get_node_positions(MAX_ORDER, DIMENSION)
    radius = 0
    graph_density = 0
    while graph_density < density_minimum:
        radius += RADIUS_INCREMENT
        graph = nx.random_geometric_graph(MAX_ORDER, radius, dim=DIMENSION, pos=pos)
        graph = remove_nodes_disconnected_from_root(graph)
        graph_density = graph.size() / MAX_SIZE
    return graph


def get_random_node_coordinates(dimension):
    coordinates = []
    for i in range(0, dimension):
        coordinates.append(random.uniform(-1, 1))
    return coordinates


def get_node_positions(order, dimension):
    positions = {0: [0] * dimension}
    for i in range(1, order):
        positions[i] = get_random_node_coordinates(dimension)
    return positions


def remove_nodes_disconnected_from_root(graph, root=0):
    root_connected_nodes = nx.node_connected_component(graph, root)
    disconnected_nodes = [n for n in graph.nodes() if n not in root_connected_nodes]
    graph.remove_nodes_from(disconnected_nodes)
    return graph


def add_values(graph, value_seed):
    set_seed(value_seed)
    vertex_values = random.choices(VALUE_POPULATION, weights=VALUE_WEIGHTS, k=graph.order())
    vertex_value_map = {vertex: vertex_values[i] for i, vertex in enumerate(graph.nodes())}
    nx.set_node_attributes(graph, values=vertex_value_map, name='value')
    return graph


def add_costs(graph, cost_seed):
    set_seed(cost_seed)
    size = graph.size()
    edge_costs = random.choices(COST_POPULATION, k=size)
    edge_cost_map = {edge: edge_costs[i] for i, edge in enumerate(graph.edges())}
    nx.set_edge_attributes(graph, values=edge_cost_map, name='cost')
    return graph


def compute_results(mine, ehp, solution, times):
    print(f'Running {solution.name} on mine {mine.id} with EHP {ehp} to get times {times}')
    with NamedTemporaryFile() as f:
        temp_mine_path = f.name
        write_mine(temp_mine_path, mine.get_graph())
        eca = get_eca(mine, ehp)
        cmd = [f'{solution.path}', f'{temp_mine_path}', f'{eca}']
        outputs = get_program_output_at_times(cmd, times)
    return outputs


def get_or_create_results(mines, ehps, solutions, times):
    all_results = []
    for mine in mines:
        for ehp in ehps:
            for solution in solutions:
                results = Result.objects.filter(
                    mine=mine,
                    ehp=ehp,
                    solution=solution,
                )
                times_to_compute = []
                for t in times:
                    try:
                        r = results.get(time=t)
                    except ObjectDoesNotExist:
                        times_to_compute.append(t)
                    else:
                        all_results.append(r)
                if not times_to_compute:
                    continue
                outputs = compute_results(mine, ehp, solution, times_to_compute)
                for time, output in outputs.items():
                    walk = read_walk_string(output)
                    graph = mine.get_graph()
                    eca = get_eca(mine, ehp)
                    score, error = score_walk(graph, eca, walk)
                    result = Result.objects.create(
                        mine=mine,
                        ehp=ehp,
                        solution=solution,
                        time=time,
                        output=output,
                        score=score,
                        error=error,
                    )
                    all_results.append(result)
    # TODO: use unions to build this query set as we go to avoid this dumbness
    return Result.objects.filter(id__in=[r.id for r in all_results])


def output_reader(p, q):
    for line in iter(p.stdout.readline, b''):
        q.put(line.decode('utf-8'))


def get_program_output_at_times(cmd, times, stop_keyword='STOP', timeout_buffer=settings.TIMEOUT_BUFFER):
    '''
    Runs an iterative program and reads stdout in real-time.
    Program output is delimited by the stop_keyword,
    and the last complete output read before each time is saved.
    Returns a dictionary mapping the given times to the respective output.

    THE RUN PROGRAM NEEDS TO MAKE SURE IT IS NOT BUFFERING ITS OWN OUTPUT.
    We attempt to prevent this for interpreted python scripts by setting the
    PYTHONUNBUFFERED flag, but other languages might need to take similar
    measures.
    '''
    if not times:
        return {}
    times = sorted(times)
    results = dict((t, None) for t in times)
    # Subprocess
    env=dict(os.environ, PYTHONUNBUFFERED='1')
    p = subprocess.Popen(
        cmd,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        env=env,
        )
    # Reading thread
    q = queue.Queue()
    t = threading.Thread(target=output_reader, args=(p, q))
    t.start()
    # Main loop
    timeout = times[-1] + timeout_buffer
    output = ''
    t0 = time.time()
    ti = 0
    td = 0
    while(td < timeout):
        while(True):
            try:
                line = q.get(block=False)
            except queue.Empty:
                break
            if(line.strip() == stop_keyword):
                results[times[ti]] = output
                output = ''
            else:
                output += line
        td = round(time.time() - t0)
        # Increase time index if we have passed the current time
        while(ti < len(times) and td > times[ti]):
            ti += 1
            if ti < len(times):
                results[times[ti]] = results[times[ti - 1]]
    p.terminate()
    t.join()
    return results


def validate_walk(graph, eca, walk):
    """
    Returns the first validation error encountered.
    returning none indicates the walk is valid.
    """
    if walk == []:
        return None
    if walk[0][0] != 0:
        return f'Walk does not start at root vertex: first step is edge {walk[0]}.'
    if walk[-1][-1] != 0:
        return f'Walk does not end at root vertex: last step is edge {walk[-1]}.'
    try:
        csum = graph.edges[walk[0]]['cost']
    except KeyError:
        return f'Walk step 0 (edge {walk[0]}) does not exist in mine.'
    last_vertex = walk[0][1]
    for i, (u, v) in enumerate(walk[1:]):
        try:
            csum += graph.edges[(u, v)]['cost']
        except KeyError:
            return f'Walk step {i+1} (edge {(u, v)}) does not exist in mine.'
        if u != last_vertex:
            return f'Walk steps {i} and {i+1} (edges {walk[i]} and {walk[i+1]}) have different stop and start vertices.'
        last_vertex = v
    if csum > eca:
        return f'The walk cost ({csum}) is strictly greater than the edge cost allowance ({eca}).'


def score_walk(graph, eca, walk):
    """Returns (score, error)"""
    if walk == []:
        return 0, None
    try:
        error = validate_walk(graph, eca, walk)
    except Exception:
        error = 'Unexpected exception validating walk.'
    if error:
        return 0, error
    walk_value = sum(nx.get_node_attributes(graph.edge_subgraph(walk), 'value').values())
    graph_value = sum(nx.get_node_attributes(graph, 'value').values())
    return round(float(walk_value) / float(graph_value), 4), error
