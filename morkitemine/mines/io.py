import json
import networkx as nx
import matplotlib.pyplot as plt
import numpy as np
from django.conf import settings
from prettytable import PrettyTable
from matplotlib import cm
from matplotlib.colors import ListedColormap


# TODO(WANT): either fix this in NX or use something else
import matplotlib.cbook
import warnings
warnings.filterwarnings("ignore", category=matplotlib.cbook.mplDeprecation)


def print_json(json_data):
    print(json.dumps(json_data, indent=4, sort_keys=True))


def get_vertex_value_tuples_from_graph(graph):
    return [(i, v) for (i, v) in graph.nodes(data='value')]


def get_edge_cost_tuples_from_graph(graph):
    return [(i, j, c) for (i, j, c) in graph.edges(data='cost')]


def read_mine(filepath):
    """
    Return a NetworkX graph from a MINE file
    """
    with open(filepath, 'r') as f:
        lines = [line.strip('\n').split(' ') for line in f.readlines()]
        vertex_lines = [line for line in lines if len(line) == 2]
        edge_lines = [line for line in lines if len(line) == 3]
        vertices = [(int(i), int(v)) for (i, v) in vertex_lines]
        edges = [(int(i), int(j), int(c)) for (i, j, c) in edge_lines]
    graph = nx.Graph()
    for (i, v) in vertices:
        graph.add_node(i, value=v)
    for (i, j, c) in edges:
        graph.add_edge(i, j, cost=c)
    return graph


def write_mine(filepath, graph):
    """
    Write a NetworkX graph to a MINE file
    """
    vertices = get_vertex_value_tuples_from_graph(graph)
    edges = get_edge_cost_tuples_from_graph(graph)
    vertex_lines = [f'{n} {v}\n' for (n, v) in vertices]
    last_edge_line = f'{edges[-1][0]} {edges[-1][1]} {edges[-1][2]}' # can't end in a newline
    edge_lines = [f'{i} {j} {c}\n' for (i, j, c) in edges[:-1]] + [last_edge_line]

    with open(filepath, 'w') as f:
        f.writelines(vertex_lines)
        f.write('STOP\n')
        f.writelines(edge_lines)
    return filepath


def read_walk(filepath):
    """
    Return edge list from a WALK file
    """
    with open(filepath, 'r') as f:
        lines = [line.strip('\n').split(' ') for line in f.readlines()]
        edge_lines = [line for line in lines if len(line) == 2]
        edges = [(int(i), int(j)) for (i, j) in edge_lines]
    return edges


def read_walk_string(walk_string):
    """
    Return edge list from a WALK string
    """
    if not walk_string:
        return []
    return [(int(x), int(y)) for x, y in [e.split(' ') for e in walk_string.split('\n') if len(e.split(' ')) == 2]]


def draw_mine(filepath, graph):
    """
    Draw a NetworkX graph as a PNG
    """
    pos = graph.nodes(data='pos')
    node_values = dict(graph.nodes.data('value'))
    node_colors = [v for v in node_values.values()]
    edge_costs = {(x,y): c for x, y, c in graph.edges.data('cost')}
    edge_colors = [c for c in edge_costs.values()]

    plt.figure(figsize=(5, 5))
    nx.draw_networkx_edges(graph, pos=pos, width=1.0, edge_color=edge_colors, edge_cmap=plt.cm.RdYlGn_r)
    nx.draw_networkx_nodes(graph, pos=pos, node_size=10, node_color=node_colors, cmap=plt.cm.Blues)

    # adding node identity labels
    # node_identities = {i: i for i in range(0,250)}
    # nx.draw_networkx_labels(graph, pos=pos, labels=node_identities, font_size=2)

    # adding node value labels
    # nx.draw_networkx_labels(graph, pos=pos, labels=node_identities, font_size=2)

    # adding edge cost labels
    # nx.draw_networkx_edge_labels(graph, pos=pos, edge_labels=edge_costs, font_size=4)

    plt.plot(pos[0][0], pos[0][1], color='black', marker='.', markersize=14)
    plt.plot(pos[0][0], pos[0][1], color='m', marker='.', markersize=10)

    # TODO: setting box to be wide and then tightening it...? make up your mind
    plt.box(False)
    b = 1.05
    plt.xlim(-b, b)
    plt.ylim(-b, b)
    plt.savefig(filepath, dpi=500, bbox_inches='tight')
    plt.close()
    return filepath


def tabulate_results(mines, ehps, solutions, times, results):
    ehps = sorted(ehps)
    score_grand_totals = {s.name: 0 for s in solutions}
    for time in times:
        table = PrettyTable()
        table.field_names = [f'Mine', f'EHP'] + [f'{s.name}' for s in solutions]
        score_totals = {s.name: 0 for s in solutions}
        for mine in mines:
            mine_name = f'Mine {mine.id}'
            for ehp in ehps:
                row = [mine_name, f'{ehp}']
                mine_name = ''  # Only print the mine name once for readability
                for solution in solutions:
                    result = results.get(mine=mine, ehp=ehp, solution=solution, time=time)
                    score = result.score
                    error = result.error
                    row += [f'{score:.4f}' if error is None else error]
                    score_totals[solution.name] += + float(score)
                table.add_row(row)
        table.add_row(['-----'] * len(table.field_names))
        table.add_row([f'Total ({time}s)', ''] + [f'{score_totals[s.name]:.4f}' for s in solutions])
        print(table)
        score_grand_totals = {k: v + score_totals[k] for k, v in score_grand_totals.items()}
    table = PrettyTable()
    table.field_names = [f'Problems', ] + [f'{s.name}' for s in solutions]
    problems = \
    f'''\
    Mines {', '.join([str(mine.id) for mine in mines])},\n\
    EHPs {', '.join([str(ehp) for ehp in ehps])},\n\
    Times {', '.join([str(time) for time in times])}\
    '''
    total_row = [problems] + [f'{score_grand_totals[s.name]:.4f}' for s in solutions]
    table.add_row(total_row)
    print(table)


def chart_results(mines, ehps, solutions, times, results, filename='results.png'):
    # Calculate
    ehps = sorted(ehps)
    scores = {s.name: {ehp: 0 for ehp in ehps} for s in solutions}
    for solution in solutions:
        for ehp in ehps:
            score = 0
            for mine in mines:
                for time in times:
                    result = results.get(mine=mine, ehp=ehp, solution=solution, time=time)
                    score += result.score
            scores[solution.name][ehp] = score
    # Chart
    plt.figure(figsize=(5, 5))
    for solution_name, solution_scores in scores.items():
        x, y = zip(*solution_scores.items())
        plt.plot(x, y, label=solution_name)
    plt.legend()
    plt.xlabel('ECA')
    plt.ylabel('Score')
    filepath = settings.MEDIA_DIR + f'/{filename}'
    plt.savefig(filepath, dpi=500)
    plt.close()
