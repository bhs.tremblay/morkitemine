import os
from django.conf import settings
from django.core.management.base import BaseCommand
from django.db.utils import IntegrityError

from mines.models import Solution


class Command(BaseCommand):
    help = f'''\
    Remove invalid (not present or not executable) solutions, and\
    register all executable files found in {settings.SOLUTIONS_DIR}\
    as solutions, using their basename without extension as the solution name.
    Existing solutions will not be updated.
    '''

    def handle(self, *args, **options):
        print(f'Removing...')
        for s in Solution.objects.all():
            dirname, filename = os.path.split(s.path)
            if not s.is_present():
                print(f'\tDeleting {s.name} ({filename} not found)')
                s.delete()
            elif not s.is_executable():
                print(f'\tDeleting {s.name} ({filename} not executable)')
                s.delete()
        print(f'Registering...')
        filenames = os.listdir(settings.SOLUTIONS_DIR)
        for filename in filenames:
            name, ext = os.path.splitext(filename)
            path = settings.SOLUTIONS_DIR + f'/{filename}'
            if os.path.isfile(path):
                if os.access(path, os.X_OK):
                    try:
                        Solution.objects.create(name=name, path=path)
                        print(f'\tRegistered {name} ({filename})')
                    except IntegrityError:
                        # Already registered
                        pass
                else:
                    print(f'\tNot registering {filename} (it is not executable)')
            else:
                print(f'\tNot registering {filename} (it is a directory)')
        print('Done!')
