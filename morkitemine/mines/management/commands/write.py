import json
import random
from tempfile import mkstemp
from django.conf import settings
from django.core.management.base import BaseCommand
from mines.models import Mine, Quarry


class Command(BaseCommand):
    help = 'Write specified mines to MINE files.'

    def add_arguments(self, parser):
        parser.add_argument('--mine', '-m', action='append', type=int)
        parser.add_argument('--quarry', '-q', action='append', type=int)

    def handle(self, *args, **options):
        mine_ids = options['mine']
        quarry_ids = options['quarry']
        if not mine_ids:
            mine_ids = []
        if not quarry_ids:
            quarry_ids = []

        mines = set()
        for mine_id in mine_ids:
            mine = Mine.objects.get(id=mine_id)
            mines.add(mine)
        for quarry_id in quarry_ids:
            q = Quarry.objects.get(id=quarry_id)
            for mine in q.mines.all():
                mines.add(mine)
        mines = list(mines)

        for mine in mines:
            mine.write()
