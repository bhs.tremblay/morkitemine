import json
import random
from tempfile import mkstemp
from django.conf import settings
from django.core.management.base import BaseCommand
from mines.models import Quarry

class Command(BaseCommand):
    help = 'Define a quarry and create its mines.'

    def add_arguments(self, parser):
        parser.add_argument('--graph-seed', '-gs', action='append', type=int, default=[])
        parser.add_argument('--value-seed', '-vs', action='append', type=int, default=[])
        parser.add_argument('--cost-seed', '-cs', action='append', type=int, default=[])
        parser.add_argument('--density', '-d', action='append', type=float, default=[])

    def handle(self, *args, **options):
        graph_seeds = options['graph_seed']
        value_seeds = options['value_seed']
        cost_seeds = options['cost_seed']
        densities = options['density']

        if not graph_seeds:
            print('At least one graph seed is required.')
            return
        if not value_seeds:
            print('At least one value seed is required.')
            return
        if not cost_seeds:
            print('At least one cost seed is required.')
            return

        if not densities:
            # Argparse default does not overwrite any elements in the default
            # list so we have to manually do this.
            densities = settings.DEFAULT_DENSITIES

        quarry, created = Quarry.objects.get_or_create(
            graph_seeds=graph_seeds,
            value_seeds=value_seeds,
            cost_seeds=cost_seeds,
            densities=densities,
        )
        if created:
            print(f'Created {quarry}')
        else:
            print(f'Fetched {quarry}')

        mines = quarry.excavate()
        print(f'Quarry has mines: {mines}')
