from django.core.management.base import BaseCommand
from django.conf import settings
from django.db import models
from mines.models import Mine, Quarry, Solution, Result, get_or_create_results
from mines.io import tabulate_results, chart_results

class Command(BaseCommand):
    help = '''Tabulate results for the given mines, quarries, ehps, and solutions.'''
    # This allows you to do weird stuff like compare solutions of different
    # competitive divisions so it's up to the caller to use this responsibly.
    # TODO: accept a flag forcing results to be re-computed.
    # TODO: accept flags specifying the kind of visualization you want, e.g. tables, charts, (animations???), etc
    def add_arguments(self, parser):
        # Mines
        parser.add_argument('--mine', '-m', action='append', type=int, default=[])
        parser.add_argument('--quarry', '-q', action='append', type=int, default=[])
        # EHPs
        parser.add_argument('--ehp', '-e', action='append', type=int, default=[])
        # Solutions
        parser.add_argument('--solution', '-s', action='append', default=[])
        parser.add_argument('--division', '-d', action='append', default=[])
        # Times
        parser.add_argument('--time', '-t', action='append', type=int, default=[])

    def handle(self, *args, **options):
        # Args
        mine_ids = options['mine']
        quarry_ids = options['quarry']
        solution_names = options['solution']
        divisions = options['division']
        ehps = options['ehp']
        times = options['time']
        # Argparse default does not overwrite any elements in the default
        # list so we have to manually do this.
        if not ehps:
            ehps = settings.DEFAULT_EHPS
        if not times:
            times = settings.DEFAULT_TIMES

        # Mines
        mines = Mine.objects.filter(id__in=mine_ids)
        quarries = Quarry.objects.filter(id__in=quarry_ids)
        for q in quarries:
            mines = mines.union(q.mines.all())

        # Solutions
        s1 = Solution.objects.filter(name__in=solution_names)
        s2 = Solution.objects.filter(division__in=divisions)
        solutions = s1.union(s2)

        if solutions.count() < 1:
            print('No solutions specified by the given arguments.')
            return
        if mines.count() < 1:
            print('No mines specified by the given arguments.')
            return

        # Results
        results = get_or_create_results(mines, ehps, solutions, times)

        # Visualization
        tabulate_results(mines, ehps, solutions, times, results)
        chart_results(mines, ehps, solutions, times, results, filename='results.png')  # TODO: accept optional flag to specify different filename/path?
