from django.conf import settings
from django.core.management.base import BaseCommand

from mines.models import Solution


class Command(BaseCommand):
    help = 'Register a specific executable file as a named solution.'

    def add_arguments(self, parser):
        parser.add_argument('name')
        parser.add_argument('filename')
        parser.add_argument('--author', '-a', default=None)
        parser.add_argument('--division', '-d', default=None)

    def handle(self, *args, **options):
        name = options['name']
        filename = options['filename']
        author = options['author']
        division = options['division']
        path = settings.SOLUTIONS_DIR + f'/{filename}'
        defaults = {'path': path}
        if author:
            defaults['author'] = author
        if division:
            defaults['division'] = division
        Solution.objects.update_or_create(
            name=name,
            defaults=defaults
        )
