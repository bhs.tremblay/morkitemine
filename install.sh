#!/bin/bash -e
#
# Create MorkiteMine database and user
#


# Create system group and user 'karl'
#
sudo /usr/sbin/groupadd --system karl &>/dev/null ||:
sudo /usr/sbin/useradd --system --gid karl --shell /bin/bash --base-dir /var/lib --create-home karl &>/dev/null ||:


# Create a postgres role 'karl' having the permission to create databases
#
KARL_USER_FOUND=$(sudo --login --user postgres psql --tuples-only --no-align --command "SELECT 1 FROM pg_roles WHERE rolname='karl'")
if [ "$KARL_USER_FOUND" != "1" ]; then
    sudo --login --user postgres createuser --createdb karl
    sudo --login --user postgres psql --command "ALTER ROLE karl SET search_path TO public"
fi


# Create postgres databases 'karl' belonging to the user karl
#
KARL_DATABASE_FOUND=$(sudo --login --user postgres psql --tuples-only --no-align --command "SELECT 1 FROM pg_database WHERE datname='karl'")
if [ "$KARL_DATABASE_FOUND" != "1" ]; then
    sudo --login --user karl createdb karl
fi

pip3 install -r requirements.pip
python3 ./morkitemine/manage.py makemigrations
python3 ./morkitemine/manage.py migrate
