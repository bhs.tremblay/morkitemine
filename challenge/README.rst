======================================
The MorkiteMine Programming Challenge
======================================

This challenge is meant to be completed in one contiguous four hour session,
but you may take up to eight.
If you continue reading this, then you should start your time.

.. note::

    If anything is unclear in this document, please email questions to
    **mine@tremblay.dev**.
    Your time invested in this is valued and I will respond as soon as possible.

Table of contents:

- `Forward`_
- `Challenge Overview`_
- `Mine Structure`_
- `Program I/O`_
- `Submission Scoring`_
- `Additional Materials`_
- `Summary`_

Forward
====================================================

The *MorkiteMine programming challenge* is to design and implement an
approximation algorithm to the *MorkiteMine optimization problem*.

The *MorkiteMine optimization problem* is to find a route through a graph
(actually, starting and stopping at the same pre-determined vertex) such that
the value of the route is maximized while keeping the cost of the route below
a given threshold.

To clarify, let me give you the fictional motivation for the problem:

    You are a miner and you need to plan a route through a mine that will soon
    collapse.
    The layout of the mine is given to you in the form
    of a graph, where the vertices are locations in the mine and the edges are
    the paths between them. The locations have mined Morkite ore ready to be
    collected, and the paths take a fixed amount of time to traverse.
    The mine only has one way in or out, so you need to plan an expedition
    into the mine starting and stopping at this entrance such that it does not
    take so long that the mine collapses. Subject to this, you want to maximize
    the amount of Morkite ore collected.

With that in mind, here is a more technical problem statement:

    You are given a graph *G* such that each vertex has an associated **value**
    and each edge has an associated **cost**.
    One vertex in particular is designatd as the **root** vertex.
    You are also given a positive integer, called the **edge cost allowance**.
    Your task is to plan a walk through *G*,
    starting and ending at the *root* vertex,
    such that the total cost of the walk is less than the edge cost allowance.
    Subject to these constraints, you want to maximize the total value of the
    walk.

There are still a lot of particular details to address, but that is the gist.
Your task for the MorkiteMine challenge is to design and implement an algorithm
for solving this problem.

Details below.

Challenge Overview
====================================================

You are to produce a program, written in any language of your choosing,
which takes as input two things:

#. a path to a file describing a Morkite mine, and
#. an integer called the **edge cost allowance**, or the **ECA** for short.

Your program should then build a walk through the mine, printing the walk
to standard out. The MINE file contains the definition of a morkite
mine in a simple text format (i.e. the *MINE format*).

Of particular note, your program **does not need to elect to stop**,
but rather it may continuously print improved walks until it is terminated
by the challenge administrator.
The last walk printed (fully) before your program has run for
a certain amount of time (e.g. 60 seconds) is the walk that will be taken
for evaluation.
As such, it is recommended that your program print each improved walk as soon as
it is built. Complete details on program input and output are below.

This final walk will be *validated* and then *scored*. If validation fails,
a score of zero is assigned. If validation passes, a score proportionate
to the total value of the walk will be assigned.
Complete details on program validation and scoring are below.

There are a few other miscellaneous rules to keep in mind:

#. Programs must be single threaded. Do not fork or run subprocesses.
#. Programs must be written without using a specialized library,
   e.g. you may use libraries for handling I/O streams,
   or for simple data structures like lists and queues,
   but not for implementing graphs, or graph algorithms like Djikstras
   or A* (that's not to say you can't use those algroithms, only that you
   must implement them yourself).
   This isn't an particularly objective rule, so if you are unsure about
   something in particular then please do not hesitate to
   email **mine@tremblay.dev** for clarification.
#. Programs should return consistent output given the same input.
   You may use 'randomness', but set a fixed seed at the start.

There should be additional materials in the tarball you received containing
this document. They are meant to assist you in developing your solution.
See the additional materials section below for details.

Information on and instructions for using these materials are also included
below.

Again, if anything is unclear in this document (or I just have a stupid typo),
please email me at **mine@tremblay.dev**.

Mine Structure
====================================================

While mines are randomly generated, they still satisfy a set of properties
and belong to a specific family of graphs.
You are encourage to exploit any of these properties to whatever extent you can.

Of particular note:

- Mines have a maximum of 250 nodes.
- Mines are **simple**, **connected**, and **undirected**.
- Mines are geometric in 2-dimensions, with the root vertex at the origin.
- Vertex values and edge costs are positive integers in the [1, 100] range.
- Vertex values are chosen randomly so that the higher the value the less
  likely it is to occur, while edge costs are chosen uniformly at random.

Due to how mines are generated, most will have a maximal 250 nodes, but not all.

**Simple** means there is at most one edge connecting one vertex to another
(no parallel edges),
and there are no edges connecting a vertex to itself (no loop edges).
**Connected** means for any two vertices there is a path in the graph connecting
them.
**Undirected** means you may "travel across" an edge in either direction.
Depending on the context, these graph properties can go without saying,
but we are explicitly stating them for clarity.

A **geometric** graph is generated by placing vertices in space and then
connecting any two vertices that are within some distance threshold of each
other. It is meant to model how roads between cities end up in the real world,
and due to its spatial nature allows for easy visualization
(mentally or otherwise).
Morkite mines in particular are generated by putting points uniformly at random
in the 2-dimensional unit cube (i.e. a square) and using a radius to realize
various densities. Of particular note, the root vertex is always at the origin.


Program I/O
====================================================

Programs need to take two positional arguments as input,
the first being a *path to a MINE file*,
and the second being the *edge cost allowance*.
MINE files define the structure of a Morkite mine in a simple text format
(this is done to support the use of any language),
and your program must read the given MINE file for the structure of the mine
it is to generate a walk in.

Programs should print their output walk(s) to standard out in a particular
format (called the *WALK* format). When the program is done printing a particular walk, it must
print ``STOP`` to signal this to the orchestrator. This keyword is expected
to delimit distinct output walks,
and **failure to print it will result in errors**.

The MINE and WALK formats are detailed below.

The MINE format
------------------------------------------------------

The vertices of a Morkite mine are described as the integers between
``0`` and ``249``.
When a mine has less than 249 vertices, it is not guaranteed that
consecutive integers are used, however **the root vertex is always** ``0``
(and is always present).

An edge is described by listing the pair of vertices it connects,
e.g. the edge connecting the vertices ``5`` and ``13`` is just called ``5 13``.
Vertices may be written in either order when specifying an edge,
e.g. ``5 13`` could also be written ``13 5``.

A MINE file describes a morkite mine by first listing all vertices with their
values (one vertex and value per line),
and then all edges with their costs (one edge and cost pair per line).
The word ``STOP`` separates the lists.

Consider the following example MINE file content::

    0 10
    1 40
    3 15
    4 15
    5 15
    STOP
    0 1 60
    0 3 30
    4 3 30
    4 5 30
    5 0 30

This file describes a mine that looks like this:

|example-mine|

In the figure, a vertex's name is written beside the vertex,
and its value is written inside the vertex.
The root vertex (i.e. ``0``) has been highlighted blue.

.. |example-mine| image:: figures/example.mine.png

You do not have to do any validation or edge case handling when reading
MINE files.
You can just expect them to be in the correct format and read them to get the
mine structure.


The WALK format
------------------------------------------------------------------------

Walks through a mine are described by sequentially printing the edges
crossed. Edges are described as they are in the MINE format,
i.e. as pairs of vertices, but unlike the MINE format, **the order they are
written matters**. In particular, the vertices should be written in the
order they are traversed.

Consider this walk through the example mine from the previous section:

|example-walk-okay|

.. |example-walk-okay| image:: figures/example.walk.okay.png

To describe this walk, your program would print the following::

    0 1
    1 0
    STOP

Suppose later your program finds this improved (i.e. higher value) walk:

|example-walk-good|

.. |example-walk-good| image:: figures/example.walk.good.png

To describe this new walk, your program would print the following::

    0 3
    3 4
    4 5
    5 0
    STOP

Recall that your program should print *each improved walk immediately*.
Here is a complete example of what a program's output might look like on
this mine, which in this example is located at the relative path
``../mines/m1.mine``::

    $ ./minesolution.o ../mines/m1.mine 120
    0 3
    3 4
    4 3
    3 0
    STOP
    0 1
    1 0
    STOP
    0 3
    3 4
    4 5
    5 0
    STOP

This final printed walk is the best walk possible for the given ECA
of 120, so the program would not print any further output (since the mine
is small it might realize it is done and elect to quit, but in general
your program will always be busy working when it gets killed).

Note these example walks all have a total cost of 120, which meets exactly the
given ECA.
If a smaller ECA had been specified, these walks would be invalid.

Also note that the order the vertices are written for each edge
respects the order they are traversed when following the edge.

.. warning::

    Make sure your program is printing to **standard output**
    and is not doing any internal output buffering.
    For example, in Python you must call ``print`` with the ``flush`` flag
    to prevent buffering.
    Depending on your choice of language, you may need to do something similar.


Submission Scoring
====================================================

Walk Value & Cost
------------------

The total value of a walk is the sum of the values of the *set* of vertices it
crosses
(each vertex contributes its value only once,
as you can't collect the same morkite from a vertex twice).

The total cost of a walk is the sum of the costs of the edges in the walk
(repeated edges contribute their cost each time they occur,
as it takes time to cross them every time you do).


Walk Validity & Scoring
-------------------------

Before a walk is scored, it is tested that it meets the following conditions:

#. The walk starts at the root vertex.
#. The walk ends at the root vertex.
#. All walk edges actually exist in the mine (you can't dig your own tunnels).
#. Walk edges start where the previous edge ended (no teleporting around).
#. The total walk cost is *less than or equal to* the given ECA.

If a walk fails to meet any of those conditions, it is considered invalid and
receives a score of zero.
Otherwise, the walk is valid, and it receives a score equal to the value of the
walk divided by the total value of the mine.

Continuing with our running example, consider the first example walk from the
previous section (the first one illustrated in a figure).
This walk visits vertices ``0`` and ``1`` so it has a total value of 10 + 40 = 50.
It crosses the edge ``0 1`` twice, so it has a total cost of 60 + 60 = 120.
The walk is valid since the ECA of 120 was not exceeded,
and it receives a score of 50/95.

The second example walk (in the next figure) visits vertices
``0``, ``3``, ``4``, and ``5``,
so it has a total value of 10 + 15 + 15 + 15 = 55.
It crosses the edges ``0 3``, ``3 4``, ``4 5``, and ``5 0``,
so it has a total cost of 30 + 30 + 30 + 30 = 120.
This walk also has a cost of 120 so it does not exceed the ECA of 120,
and it receives a score of 55/95.


Program Scoring
-------------------

Your program will be run against a set of mines,
using several ECAs for each mine,
and taking output at various times.
Your program receives a score equal to the sum of the scores of all these
outputs.

This score will rank your solution among other solutions of a similar execution
speed.

.. tip::

    The longest time a solution will be run for is 60 seconds.

Additional Materials
====================================================

There are additional materials in the directory containing this document
to help you develop your program.

In the *mines* directory, there are example ``.mine`` files
and a list of suggested ECAs to use with the mines.

In the *figures* directory, there are drawings of some of the mines
from the *mines* directory (they will have the same name but with a ``.png``
extension).
In these drawings, edges are drawn on a green-yellow-red gradient,
with green being lower cost and red being higher
cost, and vertices are drawn with different shades of blue such that the darker
a vertex is the higher its value.

In the *binaries* directory, there is a ``score_walks`` program and an
``example_solution`` program.

.. warning::

    Actually these binaries don't exist yet. Sorry :)

The ``score_walks`` program is meant to validate and score your program's output
to test it for correctness, and to see it improving.
The ``example_solution`` binary is simply an acceptable solution to the problem
for you to run and solidify your understanding of what is expected of your
solution.
To see how to use the former to test your own solution, let's walk through
using it to see how the latter performs.

First, we need to run the solution program on a mine for a given ECA
and pipe its output to a file::

    $ ./binaries/example_solution ./mines/m1.mine 250 > m1.250.walk

We must interupt the program after some time has passed.
If you want to consistently run your program for a set amount of time,
I recommend using the ``timeout`` command.
You must specify a SIGINT so that output is properly piped before quitting::

    $ timeout --signal 2 10 ./binaries/example_solution ./mines/m1.mine 250 > m1.250.walk

After running the above command, the output of running ``example_solution``
on mine ``m1.mine`` with an ECA of ``250`` for ten seconds will be in
``m1.250.walk``. Now, we can use ``score_walks`` to validate and score the walks
contained in that output.

To use ``score_walks``, give it the same arguments that were used to generate
the output (i.e. the path to the MINE file and the ECA), and the path to
the file containing the output::

    $ ./binaries/score_walks ./mines/m1.mine 250 ./m1.250.walk

It will return information about each walk in the file (validation errors
or scores).


Summary
=========

This is an attempt to summarize everything in this document into
a bullet point list of all challenge rules and solution requirements.

- Submissions may be written in any language.
- Submissions should be submitted within 4 hours of starting
  the challenge and must be submitted within 8 hours.
- Submissions must only use basic libraries, e.g. for simple data structures
  or program I/O. This isn't an objective rule and you are encouraged to
  email **mine@tremblay.dev** if you are unsure about whether something
  is okay. As a rule of thumb, you shouldn't use anything that solves
  *the* problem for you, only things that help implement your own solution
  to the problem.
- Submissions must be single-threaded.
- Submissions must consistenly return the same output given the same input
  (pseudo-randomness may be used by fixing a constant seed at the start).
- Submissions must take as input two positional arguments, i.e. a path
  to a MINE file and an integer ECA, in that order.
- Submissions must print valid walks though the mine as output.
- Submissions must print output to standard out.
- Submissions must mark the end of an output walk by printing the ``STOP`` keyword
  (even if only one walk is printed).
- Submissions should print new output each time an improved walk is found.
- Submissions should not do any internal buffering of output to ensure
  all output is registered for grading.

I'll leave one final reminder here that you are welcome to send any and all
questions about the challenge to **mine@tremblay.dev** and I will respond as
soon as possible. If anything here is unclear, missing, or otherwise wrong,
please let me know.

Good luck!
